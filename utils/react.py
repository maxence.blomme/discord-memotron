import discord

from utils.file_manager import get_file


async def send_react(channel, alias, author=None):
    file_name = get_file(alias)
    if file_name is None:
        await channel.send("*Image introuvable*")
        return
    file = open(file_name, "rb")
    discord_file = discord.File(file)
    content = "" if author is None else "{.mention} :".format(author)
    await channel.send(content=content, file=discord_file)
    discord_file.close()
    file.close()
