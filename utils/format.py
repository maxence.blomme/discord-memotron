import re

supported_formats = ["png", "jpg", "gif"]


def format(filename):
    form = re.search(r"\.[a-z]+$", filename)
    if form is None:
        return "error"
    return form.group(0)[1:]
