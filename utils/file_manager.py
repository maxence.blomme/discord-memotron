import os
import requests

from utils.format import supported_formats

libdir = "./library"


def check_directory():
    if not os.path.isdir(libdir):
        os.makedirs(libdir)


def save_file(url, name, extension):
    check_directory()
    r = requests.get(url)
    open(libdir + "/" + name + "." + extension, 'wb').write(r.content)


def get_file(alias):
    for form in supported_formats:
        path = libdir + "/" + alias + "." + form
        if os.path.isfile(path):
            return path
    return None


def delete_file(alias):
    path = get_file(alias)
    if path is not None:
        os.remove(path)
        return True
    return False


def alias_list():
    files = os.listdir(libdir)
    for i in range(len(files)):
        files[i] = os.path.splitext(files[i])[0]
    return files
