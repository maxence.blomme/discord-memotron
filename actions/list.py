import os

from utils.file_manager import alias_list

from actions.action import AbstractAction


class List(AbstractAction):

    @staticmethod
    def command():
        return "/list"

    @staticmethod
    def command_short():
        return "/l"

    @staticmethod
    def help_description():
        return "Recuperer la liste des alias enregistres"

    @staticmethod
    def help_args():
        return [""]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 1:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        msg = "*Voici les alias enregistres:*\n```\n"
        for file in alias_list():
            msg += os.path.splitext(file)[0] + "\n"
        msg += "```"
        await message.channel.send(msg)
