from random import randint

from utils.file_manager import alias_list

from actions.action import AbstractAction
from utils.react import send_react


class RandomReact(AbstractAction):

    @staticmethod
    def command():
        return "/randomreact"

    @staticmethod
    def command_short():
        return "/rr"

    @staticmethod
    def help_description():
        return "Envoie une reaction au hasard"

    @staticmethod
    def help_args():
        return [""]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 1:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        aliases = alias_list()
        await send_react(message.channel, aliases[randint(0, len(aliases))], message.author)
        await message.delete()
