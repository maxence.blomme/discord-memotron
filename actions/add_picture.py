from utils.format import supported_formats, format
from utils.file_manager import save_file, get_file

from actions.action import AbstractAction


class AddPicture(AbstractAction):

    @staticmethod
    def command():
        return "/addpicture"

    @staticmethod
    def command_short():
        return "/ap"

    @staticmethod
    def help_description():
        return "Ajouter une image a la bibliotheque"

    @staticmethod
    def help_args():
        return ["alias"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 2:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        if len(message.attachments) < 1:
            await message.channel.send("*Aucun fichier n'est attache*")
            return
        elif len(message.attachments) > 1:
            await message.channel.send("*Trop de fichiers sont attaches*")
            return

        attach = message.attachments[0]
        form = format(attach.filename)
        if form not in supported_formats:
            await message.channel.send("*Format de fichier invalide*")
            return

        if get_file(splitted[1]) is not None:
            await message.channel.send("*L'alias " + splitted[1] + " existe deja*")
            return

        save_file(attach.url, splitted[1], form)
        await message.channel.send("*Image enregistree sous l'alias " + splitted[1] + "*")
