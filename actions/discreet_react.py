from actions.react import React

from actions.action import AbstractAction


class DiscreetReact(AbstractAction):

    @staticmethod
    def command():
        return "/discreetreact"

    @staticmethod
    def command_short():
        return "/dr"

    @staticmethod
    def help_description():
        return "Envoyer discretement une image de la bibliotheque"

    @staticmethod
    def help_args():
        return ["alias"]

    @staticmethod
    async def on_call(message, client):
        await React.on_call(message, client, True)
