from utils.file_manager import delete_file

from actions.action import AbstractAction


class DeletePicture(AbstractAction):

    @staticmethod
    def command():
        return "/delpicture"

    @staticmethod
    def command_short():
        return "/dp"

    @staticmethod
    def help_description():
        return "Supprimer une image de la bibliotheque"

    @staticmethod
    def help_args():
        return ["alias"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 2:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        if delete_file(splitted[1]):
            await message.channel.send("*L'alias " + splitted[1] + " a ete correctement supprime*")
        else:
            await message.channel.send("*L'alias " + splitted[1] + " n'existe pas*")
