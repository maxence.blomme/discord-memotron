from utils.react import send_react

from actions.action import AbstractAction


class React(AbstractAction):

    @staticmethod
    def command():
        return "/react"

    @staticmethod
    def command_short():
        return "/r"

    @staticmethod
    def help_description():
        return "Envoyer une image de la bibliotheque"

    @staticmethod
    def help_args():
        return ["alias"]

    @staticmethod
    async def on_call(message, client, discreet=False):
        splitted = message.content.split()
        if len(splitted) != 2:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        await send_react(message.channel, splitted[1], None if discreet else message.author)
        await message.delete()
