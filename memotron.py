import os
import sys

import discord

from actions.action_list import ActionList
from reaction_actions.reaction_action_list import ReactionActionList

from actions.help import Help
from actions.add_picture import AddPicture
from actions.delete_picture import DeletePicture
from actions.react import React
from actions.discreet_react import DiscreetReact
from actions.random_react import RandomReact
from actions.list import List

# Check if token was given
if len(sys.argv) < 2:
    token_file = os.path.dirname(os.path.abspath(__file__)) + "/token"
    try:
        f = open(token_file)
        TOKEN = f.readline()
        f.close()
    except IOError:
        print("ERROR: Missing token")
        print("Usage: python3 main.py TOKEN [, --test]")
        exit()
else:
    TOKEN = sys.argv[1]

test_mode = False

# Check test mode
if len(sys.argv) > 2 and sys.argv[2] == "--test":
    test_mode = True

client = discord.Client()

ActionList.add_action(Help)
ActionList.add_action(AddPicture)
ActionList.add_action(DeletePicture)
ActionList.add_action(React)
ActionList.add_action(DiscreetReact)
ActionList.add_action(RandomReact)
ActionList.add_action(List)


def action_called(action, message_content):
    splitted = message_content.split()
    if len(splitted) == 0:
        return False
    first_word = splitted[0]
    full = first_word == ("test" if test_mode else "") + action.command()
    short = action.command_short() is not None and \
            first_word == ("test" if test_mode else "") + action.command_short()
    return full or short


async def parse_command(message):
    for action in ActionList.actions:
        if action_called(action, message.content):
            await action.on_call(message, client)


@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    await parse_command(message)


@client.event
async def on_raw_reaction_add(payload):
    for react_action in ReactionActionList.actions:
        await react_action.on_add(payload, client)


@client.event
async def on_raw_reaction_remove(payload):
    for react_action in ReactionActionList.actions:
        await react_action.on_remove(payload, client)


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


client.run(TOKEN)
