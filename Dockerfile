FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN mkdir /memotron

WORKDIR /memotron

RUN apk add --update alpine-sdk

ADD . /memotron/

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "memotron.py"]
