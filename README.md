# discord-memotron

Bot Discord pour réagir simplement avec des images / memes

Compatible avec images et gifs

## Lancement

Pour tout lancement, il sera nécessaire de se munir d'un token obtenable sur le
[site développeur de Discord](https://discord.com/developers/applications). Il est alors
conseillé de stocker ce token dans un fichier `token` disponible à la racine du projet, qu'il
faudra ignorer dans le projet Git afin d'éviter toute usurpation de bot.

La bibliothèque d'images réactions du bot sera toujours enregistrée dans le dossier `library`
à la racine de votre version du projet, qu'importe la méthode de lancement. Ce dossier est
ignoré par le `.gitignore`.

### Lancement avec Docker Compose (recommandé)
Une configuration Docker Compose est fournie pour simplifier au maximum le déploiement du bot.

Avant de lancer le bot avec Docker Compose, vérifiez que le token se trouve bien dans un
fichier `token` à la racine du projet.

Une fois cela effectué, déployer l'image Docker avec la commande :
```bash
docker-compose up -d
```
cela va construire et lancer l'image en arrière-plan.

Pour éteindre le bot, utiliser la commande :
```bash
docker-compose down
```

### Lancement avec Docker
Une configuration Docker est fournie pour un lancement stable et simplifié du bot.

Avant de lancer le bot avec Docker, vérifiez que le token se trouve bien dans un
fichier `token` à la racine du projet.

Une fois cela effectué, construire l'image Docker avec la commande :
```bash
docker build -t memotron .
```
puis lancer le conteneur en arrière-plan avec la commande :
```bash
docker run -d -v ./library:/memotron/library memotron
```
*Note :* Le paramètre `-v ./library:/memotron/library` peut être retiré, mais sans lui la
bibliothèque d'images réactions ne sera pas conservée d'un lancement à l'autre du bot.

### Lancement en ligne de commandes (déconseillé)
Le bot peut se lancer de deux manières différentes :
```bash
python3 memotron.py
python3 memotron.py $TOKEN
```
La première manière de lancer *nécessite* la présence du token dans un fichier `token` à la
racine du projet. La seconde permet de lancer le bot avec un token spécifique.

Il est recommandé de faire usage d'un environnement virtuel Python afin de s'assurer de la
présence constante de toutes les dépendances du projet (à l'aide de `requirements.txt`).

## Mode test

Le bot peut être lancé en mode test en ajoutant le flag `--test` à son lancement.

En mode test, chaque commande à envoyer au bot devra commencer par le préfixe `test`. Ainsi,
la commande `/help` devient `test/help`, `/h` devient `test/h`, etc...

## Implémentation


Pour ajouter une action au bot, creer un nouveau fichier Python dans le package `actions` et y creer une
classe heritant de `actions.AbstractAction` (voir la documentation dans `action.py` pour savoir ce que fait
chaque methode, l'action `actions.Help` peut etre prise comme exemple)

Une fois la classe implementee, ajouter l'action avec `ActionList.add_action(<classe>)` au debut de
`memotron.py`